package com.example.exam_template.model

data class MyMarker (val id: Long, var latitude: Float, var longitude: Float, var name: String)