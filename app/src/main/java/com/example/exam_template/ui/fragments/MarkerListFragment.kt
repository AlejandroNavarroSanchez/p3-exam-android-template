package com.example.exam_template.ui.fragments

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.exam_template.R
import com.example.exam_template.ui.adapters.MarkerAdapter
import com.example.exam_template.ui.viewmodel.MainViewModel
import com.google.android.material.floatingactionbutton.FloatingActionButton

class MarkerListFragment : Fragment(R.layout.fragment_marker_list) {
    private lateinit var toMapBtn: FloatingActionButton
    private val viewModel: MainViewModel by activityViewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //Init variables
        toMapBtn = view.findViewById(R.id.marker_list_floating_btn)

        toMapBtn.setOnClickListener {
            findNavController().navigate(R.id.mapFragment)
        }

        // Initializing RecyclerView and Adapter
        val recyclerView = view.findViewById<RecyclerView>(R.id.recyclerView)
        val adapter = MarkerAdapter(viewModel.markerList)
        recyclerView.layoutManager = LinearLayoutManager(this.context, LinearLayoutManager.VERTICAL, false)
        recyclerView.adapter = adapter

    }
}