package com.example.exam_template.ui.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.example.exam_template.R
import com.example.exam_template.model.MyMarker
import com.example.exam_template.ui.fragments.MarkerListFragmentDirections

class MarkerAdapter(val markerList: MutableList<MyMarker>) : RecyclerView.Adapter<MarkerAdapter.ViewHolder>() {

    var myList = markerList

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ViewHolder {
        val v = LayoutInflater.from(viewGroup.context).inflate(R.layout.marker_layout, viewGroup, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, i: Int) {
        viewHolder.itemTitle.text = markerList[i].name

        viewHolder.bindData(markerList[i])

        viewHolder.itemView.setOnClickListener {
            val directions = MarkerListFragmentDirections.actionMarkerListFragmentToMarkerDetail(markerList[i].id)
            Navigation.findNavController(it).navigate(directions)
        }
    }

    override fun getItemCount(): Int {
        return myList.size
    }

    @SuppressLint("ResourceAsColor")
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var itemTitle: TextView

        init {
            itemTitle = itemView.findViewById(R.id.title)
        }

        fun bindData(marker: MyMarker) {
            itemTitle.text = marker.name
        }
    }
}