package com.example.exam_template.ui.fragments

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ImageView
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.example.exam_template.R
import com.example.exam_template.ui.viewmodel.MainViewModel
import com.google.android.material.textfield.TextInputLayout

class MarkerDetail : Fragment(R.layout.fragment_marker_detail) {

    private val viewModel: MainViewModel by activityViewModels() // ViewModel
    private lateinit var image: ImageView // Image
    private lateinit var titleInput: TextInputLayout // Marker name
    private lateinit var backBtn: Button // Back button

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Init variables
        image = view.findViewById(R.id.marker_detail_image)
        titleInput = view.findViewById(R.id.marker_detail_titleInput)
        backBtn = view.findViewById(R.id.cancel_btn)

        // Loading arguments
        loadArguments()

    }

    /**
     * Method to define our arguments
     */
    private fun loadArguments() {
        val markerID = arguments?.getLong("marker_id")
        val marker = viewModel.getMarkers().filter { it.id == markerID }[0]

        titleInput.hint = marker.name

        backBtn.setOnClickListener {
            findNavController().navigate(R.id.markerListFragment)
        }
    }
}