package com.example.exam_template.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.exam_template.model.MyMarker
import com.google.android.gms.maps.model.LatLng
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase

class MainViewModel : ViewModel() {
    // Init variables
    var markerList = mutableListOf<MyMarker>()
    var markerListLD = MutableLiveData<List<MyMarker>>()
    private var counter: Long = 0
    var myLocation: LatLng = LatLng(0.0, 0.0)

    /**
     * Method to gather all data from Firebase.firestore
     */
    fun populateMarkers() {
        markerList.clear()

        Firebase.firestore.collection("users").document("alex@itb.cat")
            .collection("markers").get().addOnSuccessListener { list ->
                for (document in list) {
                    markerList.add(
                        MyMarker(
                            document.id.toLong(),
                            document.getGeoPoint("coords")!!.latitude.toFloat(),
                            document.getGeoPoint("coords")!!.longitude.toFloat(),
                            document.getString("name")!!
                        )
                    )
                }
                counter = if (markerList.size != 0)
                    markerList.last().id + 1
                else
                    0

                markerListLD.postValue(markerList)
            }
    }

    /**
     * Method to return our marker list
     */
    fun getMarkers() = markerList
}